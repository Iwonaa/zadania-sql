--4.

SELECT * FROM pracownicy
WHERE Wiek = 28;

--5.

SELECT * FROM pracownicy
WHERE Wiek <= 30;

--6.

SELECT * FROM pracownicy
WHERE Nazwisko LIKE '%ski%';

--7.

SELECT * FROM pracownicy
WHERE (ID = 1 OR ID = 4 OR ID = 7 OR ID = 18 OR ID = 20);

--8.

SELECT * FROM pracownicy
WHERE (Imie IS NOT NULL AND Nazwisko IS NOT NULL AND Wiek IS NOT NULL AND Kurs IS NOT NULL);

--9.

SELECT * FROM pracownicy
WHERE Kurs != 'DS.';

