--1.

CREATE DATABASE Moje_zainteresowania;
USE Moje_zainteresowania;

--2,3.

CREATE TABLE Zainteresowania (
    id INTEGER UNIQUE NOT NULL,
    nazwa TEXT UNIQUE NOT NULL,
    opis TEXT DEFAULT,
    data_realizacji DATE CHECK(data_realizacji < '2021-03-12')
);

--4.

INSERT INTO Zainteresowania (nazwa, opis)
VALUES ('czytanie', 'książki'),
    ('bieganie','40 minut dziennie'),
    ('jazda na rowerze', '10km dziennie'),
    ('gotowanie', 'lubię jeść'),
    ('podróże', '3 razy w roku');

--5. 

SELECT * FROM Zainteresowania;


--1.

INSERT INTO Zainteresowania (id, nazwa, opis, data_realizacji)
VALUES (6, 'pływanie', 'często', '2020-03-10');


--2.

ALTER TABLE Zainteresowania ALTER COLUMN opis VARCHAR(255);

UPDATE Zainteresowania
SET id = 4
WHERE opis = 'lubię jeść';

UPDATE Zainteresowania
SET data_realizacji = '2020-03-10'
WHERE opis = 'lubię jeść';

--3.

SELECT * FROM Zainteresowania;

--1.

DELETE FROM Zainteresowania
WHERE id IS NULL;

--2.

SELECT * FROM Zainteresowania;





