--1.

CREATE DATABASE Pracownicy;
USE Pracownicy;

SELECT * FROM pracownicy;

--2,1.

//* Załadowałam tabelę za pomocą Tasks -> Import Flat File oraz ustawiłam odpowiendie Constraints
*//

--2.

SELECT *
FROM pracownicy
WHERE wiek > 30
;

--3.

SELECT *
FROM pracownicy
WHERE wiek < 30
;

--4.

SELECT *
FROM pracownicy
WHERE imie LIKE 'K%ki';

--5.

EXEC sp_rename 'pracownicy.ID', 'NR', 'COLUMN';

--6.

SELECT * FROM pracownicy
WHERE (NR = '' OR Imie = '' OR Nazwisko = '' OR Wiek = '' OR Kurs = '');

--7.

SELECT Imie, Nazwisko, Wiek, Kurs
FROM pracownicy
WHERE kurs = 'Java';
