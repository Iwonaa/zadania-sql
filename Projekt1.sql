--1.

CREATE DATABASE SKLEP_ODZIEZOWY;
USE SKLEP_ODZIEZOWY;

--2.

CREATE TABLE Dostawca (
             Id_producenta VARCHAR(10) UNIQUE NOT NULL,
			 Nazwa_producenta VARCHAR(50) UNIQUE NOT NULL,
			 Adres_producenta VARCHAR(50) DEFAULT '',
			 NIP_producenta VARCHAR(10) UNIQUE NOT NULL
	  	);
--3.

CREATE TABLE Produkt (
            Id_produktu VARCHAR(10) UNIQUE NOT NULL,
			Nazwa_producenta VARCHAR(50),
			Nazwa_produktu VARCHAR(50),
			Opis_produktu VARCHAR(50) DEFAULT '',
			Cena_netto_zakupu DECIMAL(12,2),
			Cena_brutto_zakupu DECIMAL(12,2),
			Cena_netto_sprzedazy DECIMAL(12,2),
			Cena_brutto_sprzedazy DECIMAL(12,2),
			Procent_VAT_sprzedazy DECIMAL(12,2)
);

--4.

CREATE TABLE Zamowienia (
             Id_zamowienia VARCHAR(10)UNIQUE NOT NULL, 
			 Id_klienta VARCHAR(10) not null,
			 Data_zamowienia DATE		
);

--5.

CREATE TABLE Klient (
             Id_klienta VARCHAR(10) NOT NULL,
			 Imie VARCHAR(20),
			 Nazwisko VARCHAR(20),
			 Adres VARCHAR(50)
		
);

CREATE TABLE produkty_w_zamowieniu (
            Id_zamowienia VARCHAR(10) NOT NULL,
            Id_produktu VARCHAR(10) NOT NULL
);


--6.

INSERT INTO  Dostawca (Id_producenta, Nazwa_producenta, Adres_producenta, NIP_producenta)  VALUES
('A1', 'Numoco', 'Slowianska 15 Wroclaw','8388483941'),
('A2', 'Ingrosso', 'Sokola 33 Kielce', '9293934441'),
('A3', 'Kaskada', 'Gornicza 17 Warszawa', '9382348887'),
('A4', 'Jolmar', 'Zielna 24 Katowice', '8987890980');

INSERT INTO Produkt (Id_produktu, Nazwa_producenta, Nazwa_produktu, Opis_produktu, Cena_netto_zakupu, Cena_brutto_zakupu, Cena_netto_sprzedazy,Cena_brutto_sprzedazy,Procent_VAT_sprzedazy) VALUES
('B1', 'Numoco', 'Sukienka','poliester', 23.50, 28.91, 30.55, 37.58, 0.30),
('B2','Numoco','Szorty','poliester', 30.50, 37.52, 39.65, 48.77, 0.30),
('B3','Numoco','Marynarka','poliester', 30.50, 37.52, 39.65, 48.77, 0.30),
('B4','Numoco','Jeansy','poiester', 40.00, 49.20, 52.00, 63.96, 0.30),
('B5','Numoco','Sukienka','poliester',  23.50, 28.91, 30.55, 37.58, 0.30),
('B6','Numoco','Sukienka','poliester', 23.50, 28.91, 30.55, 37.58, 0.30),
('B7','Jolmar','T-Shirt','bawelna', 43.00, 53.89, 55.90, 68.76, 0.30),
('B8','Jolmar','T-shirt','bawelna', 43.00, 53.89, 55.90, 68.76, 0.30),
('B9','Jolmar','Top',' bawelna', 44.00, 54.12, 57.20, 70.36, 0.30),
('B10','Jolmar','T-shirt','bawelna', 43.00, 53.89, 55.90, 68.76, 0.30),
('B11','Ingrosso','Spodnie','jeansy', 98.00, 120.54, 127.40, 156.70, 0.30),
('B12','Ingrosso','Spodnie','jeansy', 98.00, 120.54, 127.40, 156.70, 0.30),
('B13','Ingrosso','Spodnie','jeansy', 98.00, 120.54, 127.40, 156.70, 0.30), 
('B14','Ingrosso','Spodnie','jeansy', 98.00, 120.54, 127.40, 156.70, 0.30),
('B15','Kaskada','Bluzka','satyna', 66.00, 81.18, 85.80, 105.53, 0.30),
('B16','Kaskada','Bluzka','satyna', 66.00, 81.18, 85.80, 105.53, 0.30),
('B17','Kaskada','Bluzka','satyna', 66.00, 81.18, 85.80, 105.53, 0.30),
('B18','Kaskada','Kombinezon','w kwiaty', 65.00, 79.95, 84.50, 103.94, 0.30),
('B19','Kaskada','Kombinezon','dlugi',65.00, 79.95, 84.50, 103.94, 0.30),
('B20','Kaskada','Kombinezon','na lato', 65.00, 79.95, 84.50, 103.94, 0.30);

INSERT INTO Zamowienia( Id_zamowienia, Id_klienta, Data_zamowienia) VALUES
('C1', 'D1','2020-04-05'),
('C2', 'D2','2020-04-05'),
('C3', 'D3','2020-04-05'),
('C4', 'D4','2020-04-06'),
('C5', 'D5','2020-04-06'),
('C6', 'D6','2020-08-07'),
('C7', 'D7','2020-06-08'),
('C8', 'D8','2020-06-08'),
('C9', 'D9','2020-04-10'),
('C10','D10','2020-04-11');


INSERT INTO Produkty_w_zamowieniu (Id_zamowienia, Id_produktu) Values
('C1', 'B5'),
('C1', 'B8'),
('C1', 'B15'),
('C2', 'B20'),
('C3', 'B6'),
('C4', 'B7'),
('C5', 'B7'),
('C5', 'B19'),
('C6', 'B9'),
('C7', 'B9'),
('C8', 'B1'),
('C9', 'B2'),
('C10', 'B3');

INSERT INTO Klient(Id_klienta, Imie, Nazwisko, Adres) Values
('D1', 'Anna','Nowak', 'Sowia 13, Tychy'),
('D2', 'Maja', 'Lis', 'Sokola 60, Katowice'),
('D3', 'Kaja', 'Kowalska', 'Grna 33, Opole'),
('D4', 'Alicja', 'Wilk', 'Maa 18, Krakw'),
('D5','Sylwia','Lisowska','Konopnicka 29, Gdask'),
('D6', 'Justyna','Gil','Fabryczna 23, Szczecin'),
('D7','Nina', 'Kowolska', 'Soneczna 13, Wrocaw'),
('D8', 'Anna', 'Kij', 'Wiejska 40, Bytom'),
('D9', 'Agnieszka', 'Zych', 'Dua 12, Kielce'),
('D10', 'Julia', 'Zych', 'Maa 15, Kielce');


--7.

--Produkt - dostawca

ALTER TABLE Dostawca ADD PRIMARY KEY (Nazwa_producenta);

ALTER TABLE Produkt
ADD FOREIGN KEY (Nazwa_producenta) REFERENCES Dostawca(Nazwa_producenta);

--Zamowienia -klient

ALTER TABLE Klient ADD PRIMARY KEY (Id_klienta);

ALTER TABLE Zamowienia ADD FOREIGN KEY (Id_klienta) REFERENCES Klient(Id_klienta);

--Produkt -zamowienie

ALTER TABLE Zamowienia ADD PRIMARY KEY (Id_zamowienia);

ALTER TABLE Produkty_w_zamowieniu ADD PRIMARY KEY (Id_zamowienia, Id_produktu);

ALTER TABLE Produkty_w_zamowieniu ADD FOREIGN KEY (Id_zamowienia) REFERENCES Zamowienia (Id_zamowienia);

ALTER TABLE Produkty_w_zamowieniu ADD FOREIGN KEY (Id_produktu) REFERENCES Produkt(Id_produktu);


--8.

SELECT p.Nazwa_produktu, d.Id_producenta, d.Nazwa_producenta, d.Adres_producenta, d.NIP_producenta
FROM Produkt p
INNER JOIN Dostawca d
ON d.Nazwa_producenta = p.Nazwa_producenta
WHERE Id_producenta = 'A1';

--9.

SELECT p.Nazwa_produktu 
FROM Produkt p
INNER JOIN Dostawca d
ON d.Nazwa_producenta = p.Nazwa_producenta
WHERE Id_producenta = 'A1'
ORDER BY Nazwa_produktu ASC;

--10.

SELECT AVG(cena_brutto_sprzedazy) AS 'Srednia cena za produkt'
FROM Produkt P
INNER JOIN Dostawca d
ON p.Nazwa_producenta = d.Nazwa_producenta
WHERE Id_producenta = 'A1';


--11.

SELECT  id_produktu, nazwa_produktu, Cena_brutto_sprzedazy,
CASE
	WHEN NTILE(2) OVER (Order BY cena_brutto_sprzeday) = 1 THEN 'tanie'
	WHEN NTILE(2) OVER (Order BY cena_brutto_sprzeday) = 2 THEN 'drogie'
	END AS 'Ocena'
FROM Produkt
WHERE Nazwa_producenta = 'Numoco';


--12.


SELECT Nazwa_produktu 
FROM Produkt p
INNER JOIN produkty_w_zamowieniu z
ON z.Id_produktu = p.Id_produktu;

--13.

SELECT TOP 5 Nazwa_produktu 
FROM Produkt p
INNER JOIN produkty_w_zamowieniu z
ON z.Id_produktu = p.Id_produktu;

--14.

SELECT SUM(Cena_brutto_sprzedazy) AS 'Wartosc wszystkich zamowien'
FROM Produkt p
INNER JOIN produkty_w_zamowieniu z
ON p.Id_produktu = z.Id_produktu;

--15.

SELECT p.Nazwa_produktu, z.Data_zamowienia, w.Id_produktu, w.Id_zamowienia
FROM Produkt p
INNER JOIN Produkty_w_zamowieniu w
ON p.Id_produktu = w.Id_produktu
INNER JOIN Zamowienia z
ON w.Id_zamowienia = z.Id_zamowienia
ORDER BY Data_zamowienia ASC;

--16.

Select * From Produkt
WHERE (Id_produktu IS NULL OR Nazwa_produktu IS NULL OR Nazwa_producenta IS NULL OR 
Opis_produktu IS NULL OR cena_netto_zakupu IS NULL OR cena_brutto_zakupu IS NULL OR
cena_netto_sprzedazy IS NULL OR cena_brutto_sprzedazy IS NULL OR Procent_VAT_sprzedazy IS NULL);


--17.

SELECT Nazwa_produktu, Cena_brutto_sprzedazy
FROM Produkt p
INNER JOIN produkty_w_zamowieniu w
ON p.Id_produktu = w.Id_produktu
WHERE Nazwa_produktu = (SELECT TOP 1 Nazwa_produktu
                        FROM produkty_w_zamowieniu w
						INNER JOIN Produkt p
						ON w.Id_produktu = p.Id_produktu
                        GROUP BY Nazwa_produktu
                        ORDER BY COUNT(*) DESC);

--18.

SELECT TOP 1 data_zamowienia, count(Id_zamowienia) as ilosc_zamowien
FROM Zamowienia
GROUP BY Data_zamowienia
ORDER BY ilosc_zamowien DESC;
