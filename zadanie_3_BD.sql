--4.

SELECT * FROM pracownicy
WHERE Nazwisko = 'Kowalczyk';

--5.

SELECT *
FROM pracownicy
WHERE Wiek BETWEEN 30 AND 40;

--6.

SELECT * FROM pracownicy
WHERE Nazwisko NOT LIKE '%AND%';

--7.

SELECT * FROM pracownicy
WHERE ID BETWEEN 1 AND 7;

--8.

SELECT *
FROM pracownicy
WHERE Wiek is null or Kurs is null or Imie is null or Nazwisko is null;

--9.

SELECT * FROM pracownicy
WHERE Kurs IS NULL;

