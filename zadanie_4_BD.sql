--4.

SELECT * FROM pracownicy
WHERE imie = 'Anna';

--5.

SELECT * FROM pracownicy
WHERE imie IS NULL;

--6.

SELECT Kurs FROM pracownicy
WHERE wiek between 30 and 40
AND kurs IS NOT NULL;

--7.

SELECT wiek FROM pracownicy
WHERE ID BETWEEN 1 AND 7;

--8.

SELECT * FROM pracownicy
WHERE Wiek IS NULL;

--9.

EXEC sp_rename 'pracownicy.Kurs', 'Szkolenie', 'COLUMN';

select * from pracownicy;