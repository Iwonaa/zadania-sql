--1.

CREATE DATABASE Lista_Pracowników;
USE Lista_Pracowników;

--2,3.

CREATE TABLE Pracownicy (
    id INTEGER UNIQUE NOT NULL,
    imie TEXT NOT NULL,
    nazwisko TEXT NOT NULL,
    stanowisko TEXT NOT NULL,
    dzial TEXT NOT NULL

);

--4.

INSERT INTO Pracownicy (id, imie, nazwisko, stanowisko, dzial)
VALUES (1, 'Anna', 'Lis', 'księgowa', 'finanse'),
       (2, 'Ewa', 'Kowal', 'programista', 'IT'),
       (3, 'Piotr', 'Chyb', 'analityk', 'finanse'),
       (4, 'Kamil', 'Nowak', 'programista', 'IT'),
       (5, 'Maja', 'Kij', 'księgowa', 'finanse'),

--5. 

SELECT * FROM Pracownicy;

--6.

ALTER TABLE Pracownicy ADD data_zatrudnienia DATE CHECK(data_zatrudnienia < '2021-03-12');

--7.

INSERT INTO Pracownicy (id, imie, nazwisko, stanowisko, dzial, data_zatrudnienia)
VALUES (6, 'Alicja', 'Maj', 'analityk', 'finanse', '2019-03-04');

--8.

SELECT * FROM Pracownicy;

--9. 

UPADETE Pracownicy
SET data_zatrudnienia = '2018-03-09'
WHERE id = 1;

UPADETE Pracownicy
SET data_zatrudnienia = '2018-03-09'
WHERE id = 2;

UPDATE Pracownicy
SET data_zatrudnienia = '2018-09-09'
WHERE id = 3;

UPDATE Pracownicy
SET data_zatrudnienia = '2018-08-09'
WHERE id = 4;

UPDATE Pracownicy
SET data_zatrudnienia = '2018-03-09'
WHERE id = 5;

--10.

SELECT * FROM Pracownicy;




