CREATE DATABASE aggregates;

CREATE TABLE aggregates_batman (
             id INTEGER PRIMARY KEY NOT NULL,
			 first_name TEXT,
			 last_name TEXT,
			 sex CHAR(1),
			 age INTEGER,
			 price DECIMAL(12,2),
			 start_date DATE,
			 gift TEXT,
);

INSERT INTO aggregates_batman VALUES (1, 'Alicja', 'Rogal', 'F', 16, 100.25,'2020-01-02', 'rower' );
INSERT INTO aggregates_batman VALUES (2, 'Iwona', 'Kowalska', 'F', 33, 56.58, '2020-01-03', 'komputer');
INSERT INTO aggregates_batman VALUES (3, 'Igor', 'Kowalski', 'M', 50, 68.00, '2020-01-04', 'karty');
INSERT INTO aggregates_batman VALUES (4, 'Kamil', 'Juszczak', 'M', 50, 40.87, '2020-01-05', 'pi�ka'); 
INSERT INTO aggregates_batman VALUES (5, 'Konrad', 'Kowal', 'M', 18, 32.63, '2020-01-06', 'herbata' );
INSERT INTO aggregates_batman VALUES (6, 'Iwona', 'Feniks', 'F', 35, 78.98, '2020-01-07', 'okno' );
INSERT INTO aggregates_batman VALUES (7, 'Robert', 'Lew', 'M', 40, 120.32, '2020-01-08', 'drzwi');
INSERT INTO aggregates_batman VALUES (8, 'Tomasz', 'Nowak', 'M', 60, 150.00, '2020-01-09', 'korona');
INSERT INTO aggregates_batman VALUES (9, 'Aldona', 'Buk', NULL, NULL, 121.25, '2020-01-10', 'wycieczka');


--a.

SELECT
DAY(START_DATE) AS DZIEN,
MONTH(START_DATE) AS MIESIAC,
YEAR(START_DATE) AS ROK
FROM aggregates_batman;

--b.
SELECT START_DATE, DATEADD(day,3,start_date) AS final_date from aggregates_batman;


--c.

SELECT CAST(GETDATE() AS date);

--d.

SELECT month(getdate());


--e.

SELECT start_date,
datepart(wk,start_date) AS tydzien,
datepart(m,start_date) AS miesiac,
datepart(q,start_date) AS kwartal,
datepart(DAYOFYEAR,start_date) AS dzien
FROM aggregates_batman;

  